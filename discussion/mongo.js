/*
	Data Model Design - step where the structure of all your service/application's data is decided, much like a blueprint is needed before building a house can begin.

	Database: All the collections compiled for a service/application's data
	e.g. a filing cabinet
	Collections: separate categories compiled for a service/application's data
	e.g. each drawer of a filing cabinet
		user collection
		products collection
		orders collection

	Documents: each specific, individual record
	e.g. folders inside drawers
		specific user
		specific product
		specific order

	Sub-documents: specific information about a record that is compiled simliarly to a document, but is inside of a document itself
	e.g. file inside a folder

	Fields: each record's specific information + their data type
	e.g. file/folder content


*/

// When starting to model your data, the very first question you need to ask is "What kind of information will I need to save to make my app/service work"?

/*
e.g. a blog
	- Users
		- ID (MongoDB automatically adds an ID to all documents/records)
		- username: string
		- password: string
		- email: string
		- isAdmin: boolean
	- Blog Posts/Content
		- ID (MongoDB automatically adds an ID to all documents/records)
		- title: string
		- body: string
		- datePosted: date
		- author: string or MongoDB Object ID
*/

// Embedded vs Referebce Data

/*
	When data is embedded, it is included as a direct part (called a subdocument) of any other data that has a relationship with it

	ex. Users document:
	{
		_id: <objectId1>,
		user: "123xyz",
		address: {
			street: "123 Street st",
			city: "New York",
			country: "United States"
		}
	}

	When data is referenced, any other data that needs to be linked must be referenced, ideally via ID.

	ex. Users document:
	{
		_id: <objectId1>,
		user: "123xyz"
	}

	Addresses Document:
	address: {
		_id: <objectID2>,
		userID: <objectId1>,
		street: "123 Street st",
		city: "New York",
		country: "United States"
		}
*/



When to use Embedded vs Referenced:

it often only has "suggestions" instead of rules. if you believe that your data is more readable/makes more sense in one way, go ahead and do so. Generally, embedded data is easier to use and understand, EXCEPT for one scenario:
if you expect the data that yo uwill embed will continuously grow in size, it is safer to move that data outside the doucment and into its own collection, then just use references.

/*Users document:
	{
		_id: <objectId1>,
		user: "123xyz",
		orders: [
			{
				orderdata
			}
				]
	}

Product document:
{
	_id: <objectId2>,
	productName: "Colgate",
	category: "Toiletries",
	price: 50,
	isAvailable: true
}

{
	_id: <objectId3>,
	productName: "Cheetos",
	category: "snacks",
	price: 5,
	isAvailable: true
}

Order document:
	{
		{
		_id: <objectId1>,
		products: [
			productID: <objectId2>,
			qty: 2
		},
		{
			productID: <objectId3>,
			qty: 2
		}
			
		]
	}
*/


