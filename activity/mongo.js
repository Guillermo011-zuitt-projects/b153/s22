/*

	- Users document
		- ID (MongoDB automatically adds an ID to all documents/records)
		- username: string
		- password: string
			- contact
				- email: string
				- contact number: number
			- address: 
				- shipping address: string
				- billing address: string
	- Product document
		- ID (MongoDB automatically adds an ID to all documents/records)
		- productName: string
		- description: string
		- datePosted: date
		- inventory: number
		- isAvailable - boolean
	- Order document
		- ID (MongoDB automatically adds an ID to all documents/records)
		- productID: (Product Document ID)
		- userID: (Users Document ID)
		- productName: string
		- quantity: number
		- datePurchased: date


		*/